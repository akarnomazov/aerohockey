﻿using System;
using UnityEngine;

public class GameControl
{
    private GameModel model;

    public GameControl(GameZonesView zonesView)
    {
        model = new GameModel();

        zonesView.UpdatePointsEvent += (player, points) => UpdatePointsHandler(player, points);
    }

    private void UpdatePointsHandler(int playerIndex, int points) =>
        model.SetPoints(playerIndex, points);
}
