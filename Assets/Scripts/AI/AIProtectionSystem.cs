﻿using UnityEngine;
using Zenject;

public class AIProtectionSystem : MonoBehaviour
{
    [Range(5, 50)][SerializeField] private float impulsePower;
    [SerializeField] private float angle;
    [SerializeField] private float bangTimeOut;
    [SerializeField] private bool isEnabled = true;
    private AIZone zone;

    private void OnValidate()
    {
        if (angle < 1) angle = 1;
        if (angle > 179) angle = 179;
        if (bangTimeOut < 0.1f) bangTimeOut = 0.1f;
    }

    [Inject]
    private void Construct(AIZone zone)
    {
        this.zone = zone;
    }

    public void EnableProtectionSystem() => InvokeRepeating("BangRandomPuck", 0f, bangTimeOut);
    public void DisableProtectionSystem() => CancelInvoke("BangRandomPuck");

    //Invoked method
    public void BangRandomPuck()
    {
        if (!isEnabled) return;
        var randomPuck = zone.GetRandomPuck();
        var randomAngle = (Random.Range(-angle, angle) - 90f) * Mathf.Deg2Rad;
        var direction = new Vector2(Mathf.Cos(randomAngle), Mathf.Sin(randomAngle));
        if (randomPuck == null) return;
        randomPuck.AddForce(impulsePower * direction, ForceMode2D.Impulse);
    }
}