using UnityEngine;
using Zenject;

public class GameSceneInstaller : MonoInstaller
{
    [SerializeField] private GameZonesView gameZonesView;
    [SerializeField] private AIZone aiZone;
    [SerializeField] private AIProtectionSystem protectionSystem;

    public override void InstallBindings()
    {
        Container
            .Bind<GameZonesView>()
            .FromInstance(gameZonesView)
            .AsSingle()
            .NonLazy();

        Container
            .Bind<GameControl>()
            .AsSingle()
            .NonLazy();

        Container
            .Bind<AIZone>()
            .FromInstance(aiZone)
            .AsSingle()
            .NonLazy();
        
        Container
            .Bind<AIProtectionSystem>()
            .FromInstance(protectionSystem)
            .AsSingle()
            .NonLazy();

        Container
            .Bind<AIControl>()
            .AsSingle()
            .NonLazy();
    }
}