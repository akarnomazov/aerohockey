﻿using System;
using UnityEngine;

public class PlayerZone : MonoBehaviour
{
    [SerializeField] private int pucksCount;
    public Action<int> UpdatePuckCountEvent;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PuckMover>())
            EnterHandler(collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<PuckMover>())
            ExitHandler(collision);
    }

    protected virtual void EnterHandler(Collider2D collision)
    {
        pucksCount++;
        UpdatePuckCountEvent.Invoke(pucksCount);
    }

    protected virtual void ExitHandler(Collider2D collision)
    {
        pucksCount--;
        UpdatePuckCountEvent.Invoke(pucksCount);
    }
}
