﻿using System;
using System.Collections;
using UnityEngine;

public class GameModel
{
    private int[] playerPoints = new int[2];

    public void SetPoints(int playerIndex, int points)
    {
        playerPoints[playerIndex] = points;
        if (points == 0)
            Debug.Log($"Player {playerIndex + 1} win");
    }
}
