using System;
using TMPro;
using UnityEngine;

public class GameZonesView : MonoBehaviour
{
    [SerializeField] private PlayerZone[] playerZones = new PlayerZone[2];
    [SerializeField] private TMP_Text[] playerPoints = new TMP_Text[2];
    public Action<int, int> UpdatePointsEvent;

    private void Awake()
    {
        for (int i = 0; i < playerZones.Length; i++)
        {
            int tmpIndex = i;
            playerZones[i].UpdatePuckCountEvent += points => UpdatePoints(tmpIndex, points);
        }
    }

    private void UpdatePoints(int playerIndex, int pointsValue)
    {
        UpdatePointsEvent.Invoke(playerIndex, pointsValue);
        int tmpIndex = (playerIndex == 0) ? 1 : 0;
        playerPoints[tmpIndex].text = pointsValue.ToString();
    }
}
