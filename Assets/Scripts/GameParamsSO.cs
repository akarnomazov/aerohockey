using UnityEngine;

[CreateAssetMenu(fileName = "NewGameConfig", menuName = "Game config")]
public class GameParamsSO : ScriptableObject
{
    public float 
        ImpulsePowerScale, 
        puckLinearDrag, 
        puckBouncess, 
        obstacleBouncess;
}
