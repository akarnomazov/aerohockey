using UnityEngine;

public class PuckMover : MonoBehaviour
{
    private readonly float deadZone = 0.1f;
    [SerializeField] private float impulseKoef = 10f;
    private Vector2 beganPos, endPos;
    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update() => InputHandle();

    //TODO: Fix punch action
    private void InputHandle()
    {
#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    beganPos = Camera.main.ScreenToWorldPoint(touch.position);
                    break;
                case TouchPhase.Ended:                                          
                    endPos = Camera.main.ScreenToWorldPoint(touch.position);
                    SwipeHandle();
                    break;
                default:
                    break;
            }
        }
#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
            beganPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonUp(0))
        {
            endPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            SwipeHandle();
        }

        if (Input.GetKeyDown(KeyCode.F))
            ResetScene();
#endif
    }

    private void SwipeHandle()
    {
        if (Vector2.Distance(beganPos, endPos) < deadZone) return;
        if (Vector2.Distance(endPos, transform.position) > 0.5f) return;
        
        SetImpulse();
    }

    private void SetImpulse()
    {
        var heading = endPos - beganPos;
        var distance = heading.magnitude;
        rb.AddForce(impulseKoef * heading/distance, ForceMode2D.Impulse);
    }

    private void ResetScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
}
