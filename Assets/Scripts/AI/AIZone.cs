﻿using System.Collections.Generic;
using UnityEngine;

public class AIZone : PlayerZone
{
    private List<Rigidbody2D> pucksList = new List<Rigidbody2D>();

    protected override void EnterHandler(Collider2D collision)
    {
        pucksList.Add(collision.GetComponent<Rigidbody2D>());
        base.EnterHandler(collision);
    }

    protected override void ExitHandler(Collider2D collision)
    {
        pucksList.Remove(collision.GetComponent<Rigidbody2D>());
        base.ExitHandler(collision);
    }

    public Rigidbody2D GetRandomPuck()
    {
        if (pucksList.Count < 1) return null;
        return pucksList[Random.Range(0, pucksList.Count)];
    }

    public void EnableProtectionSystem() => InvokeRepeating("BangRandomPuck", 0f, 2f);
    public void DisableProtectionSystem() => CancelInvoke("BangRandomPuck");

    //Invoked method
    public void BangRandomPuck()
    {
        if (pucksList.Count < 1) return;
        var randomPuck = pucksList[Random.Range(0, pucksList.Count)];
        randomPuck.AddForce(15f * Vector2.down, ForceMode2D.Impulse);
    }
}
